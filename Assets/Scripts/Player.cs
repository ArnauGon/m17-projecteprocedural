using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 5f;
    [SerializeField]
    float jumpForce = 5f;
    
    [SerializeField]
    private bool isGrounded;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        IsGrounded();
    }
    
     void IsGrounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.down, 1f);
        if (hit.collider.transform.tag == "Land")
        {
            Debug.DrawRay(transform.position, Vector2.down, Color.red);

            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
        
    public void Move() {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"),0f,0f);
        transform.position += move * Time.deltaTime * moveSpeed;
    }

    public void Jump() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (isGrounded)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
            }
        }
    }
}
